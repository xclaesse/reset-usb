Based on https://gist.github.com/guss77/1356e5f3be4f2acbc73053cc6d3c0b1c

1. Copy this file to /usr/local/bin/reset-usb.sh

2. Create /etc/systemd/system/reset-usb.service:
```
[Unit]
Description=Automatically reset XHCI USB bus on error
[Service]
ExecStart=/usr/local/bin/reset-usb.sh
[Install]
WantedBy=multi-user.target
```

3. Start the service
```
sudo systemctl daemon-reload
sudo systemctl enable reset-usb
sudo systemctl start reset-usb
```
