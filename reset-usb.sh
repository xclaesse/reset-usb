#! /bin/bash

dmesg -w | while read line; do
  [[ "$line" =~ "Abort failed to stop command ring" ]] || continue
  (
    sleep 15 # let the busses settle
    ids="$(ls /sys/bus/pci/drivers/xhci_hcd/ | grep '[0-9]')"
    echo "Resetting USB"
    for id in $ids; do
      echo -n $id > /sys/bus/pci/drivers/xhci_hcd/unbind
    done
    sleep 1
    for id in $ids; do
      echo -n $id > /sys/bus/pci/drivers/xhci_hcd/bind
    done
    echo "Done resetting USB"
  )&
done
